function validateForm() {
	var name = document.forms["contact_us"]["Name"].value;
	var email = document.forms["contact_us"]["Email"].value;
	var phone = document.forms["contact_us"]["Phone"].value;
	var reasonForInquiry = document.forms["contact_us"]["inquiry_reason"].value;	
	var additionalInfo = document.forms["contact_us"]["additional_information"].value;
	
	if (name == null || name ==""){
		alert("Name must be filled out");
		return false;
	} else if ((email || phone) == null || (email || phone) == ""){
		alert("Please fill in a form of contact information: Email, Phone");
		return false;
	} else if (reasonForInquiry == "Other"){
		if (additionalInfo == null || additionalInfo == ""){
			alert("Please fill out Additional Information");
			return false;
		}
	} else if (document.forms["contact_us"]["contact_availability"][0].checked == false && document.forms["contact_us"]["contact_availability"][1].checked == false && document.forms["contact_us"]["contact_availability"][2].checked == false && document.forms["contact_us"]["contact_availability"][3].checked == false && document.forms["contact_us"]["contact_availability"][4].checked == false)
	{
		alert("Please check the day(s) that you are available for contact");
		return false;
	}
}



